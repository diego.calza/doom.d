;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — Lele's preferences
;; :Created:   gio 16 dic 2021, 19:32:21
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2021, 2022 Lele Gaifax
;;

;; NOTE: you do not need to run 'doom sync' after modifying this file!

;; Tell Emacs who I am, it uses the information in several places
(setq! user-full-name "Lele Gaifax"
       user-mail-address "lele@metapensiero.it")

;; Default theme, one can always load a different one with `load-theme'
(setq! doom-theme 'tsdh-light)

;; Alternative theme, activable with `C-c t T'
(setq! my/other-theme 'tsdh-dark)

;;;
;;; IRC
;;;

(after! circe
  (defun +irc--add-circe-buffer-to-persp-h ()
    (when (and (bound-and-true-p persp-mode)
               (+workspace-exists-p +irc--workspace-name))
      (let ((ws (persp-get-by-name +irc--workspace-name))
            (buf (current-buffer)))
        ;; Add a new circe buffer to irc workspace if not already there
        (unless (cl-find buf (safe-persp-buffers ws))
          (persp-add-buffer buf ws nil nil)))))

  (set-irc-server! "orphu.arstecnica.it"
    `(:port 7777
      :nick "lelit"
      :realname ,user-full-name
      :user ,(+pass-get-user "IRC/orphu.arstecnica.it")
      :pass (lambda (&rest _) (+pass-get-secret "IRC/orphu.arstecnica.it"))
      :channels ("#celery"
                 "#darcs"
                 "#etour"
                 "#linuxtrent"
                 "#pglast"
                 "#postgresql"
                 "#rafanass"
                 "#sqlalchemy"
                 )))
  (enable-lui-logging-globally))


;;;
;;; NEWS
;;;

(after! gnus
  (setq! gnus-select-method '(nntp "gmane"
                                   (nntp-address "news.gmane.io")
                                   (nnir-search-engine nntp))))

(after! gnus-msg
  (setq! gnus-posting-styles
         '((".*"
            (signature-file "~/.signature")
            (name "Lele Gaifax")
            (organization "Nautilus Entertainments"))
           ("^mail.+:"
            (name "Lele Gaifax")
            (organization "Nautilus Entertainments")
            (signature-file "~/.mail-signature")))))


;;;
;;; EMAIL
;;;

(after! notmuch
  ;; Restore my preferred result order, reverting Doom choice
  (setq-default notmuch-search-oldest-first t)

  ;; Reset undesiderable left-side layout for the hello page, like the
  ;; following:
  ;; (set-popup-rule! "^\\*notmuch-hello" :side 'left :size 30 :ttl 0)
  (setq +popup--display-buffer-alist
        (cl-delete-if (lambda (elem) (equal elem "^\\*notmuch-hello"))
                      +popup--display-buffer-alist
                      :key #'car :test))
  (when +popup-mode
    (setq display-buffer-alist +popup--display-buffer-alist))

  (setq!
   ;; I have configured notmuch.hooks.preNew to execute mbsync, so nothing else
   ;; is needed
   +notmuch-sync-backend "notmuch new"

   ;; This is not the default width, just the reflow limit for very long lines
   notmuch-wash-wrap-lines-length 90

   ;; Set a reasonable set of initial searches
   notmuch-saved-searches '((:name "Inbox"     :query "tag:inbox"    :key "i")
                            (:name "Unread"    :query "tag:unread"   :key "u")
                            (:name "Flagged"   :query "tag:flagged"  :key "f")
                            (:name "Today"     :query "date:today"   :key "t")
                            (:name "Sent"      :query "tag:sent"     :key "s")
                            (:name "Drafts"    :query "tag:draft"    :key "d")
                            (:name "All mail"  :query "*"            :key "a"))

   ;; Setup Emacs email sending mechanism to use msmtp
   sendmail-program "msmtp"
   mail-specify-envelope-from t
   message-sendmail-envelope-from 'header

   ;; Automatic FCC accordingly with the sender address
   notmuch-fcc-dirs '(("lele@etour.tn.it" . "etour/Sent +sent -inbox")
                      ("lele@metapensiero.it" . "metapensiero/Sent +sent -inbox")
                      ("lelegaifax@gmail.com" . "gmail/Sent +sent -inbox")))

  (map!
   :localleader
   :map notmuch-show-mode-map
   :desc "Mark as deleted" "d" #'+notmuch/show-delete)

  ;; Redefine, to avoid automatic/immediate kill of the buffer
  (defun +notmuch/update ()
    "Sync notmuch emails with server."
    (interactive)
    (with-current-buffer (compile (+notmuch-get-sync-command))
      (add-hook
       'compilation-finish-functions
       (lambda (buf status)
         (if (equal status "finished\n")
             (progn
               ;;(kill-buffer buf)
               (notmuch-refresh-all-buffers)
               (message "Notmuch sync successful"))
           (user-error "Failed to sync notmuch data")))
       nil
       'local))))


;;;
;;; PatchDB
;;;

(after! projectile
  ;; These are common associations in PatchDB context
  (add-to-list 'projectile-other-file-alist '("sql" "rst" "py"))
  (add-to-list 'projectile-other-file-alist '("rst" "sql" "py")))
