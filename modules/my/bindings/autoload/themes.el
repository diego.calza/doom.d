;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — Themes sugar
;; :Created:   dom 9 gen 2022, 09:21:47
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2022 Lele Gaifax
;;

(defvar my/other-theme nil
  "A symbol representing an alternative Emacs theme.

When this is set (usually in your $DOOMDIR/config.el), the
function `my/switch-to-other-theme' will use it to toggle between
that theme and the current one, basically swapping the values of
`doom-theme' and `my/other-theme'.")

;;;###autoload
(defun my/switch-to-other-theme ()
  "Toggle current theme between `doom-theme' and `my/other-theme'.

If `my/other-theme' is set, then activate it and replace that
value with the previous active theme (that is, `doom-theme'), so
that repeatedly calling this function will toggle between those
two themes."
  (interactive)
  (when my/other-theme
    (let ((current-theme doom-theme))
      (load-theme my/other-theme t)
      (setq my/other-theme current-theme))))
