;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — Symbol helpers
;; :Created:   ven 10 dic 2021, 13:12:58
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2021 Lele Gaifax
;;

;;;###autoload
(defun backward-symbol (&optional arg)
    "Move backward until encountering the beginning of a symbol.
With argument ARG, do this that many times.
If ARG is omitted or nil, move point backward one word."
    (interactive "^p")
    (forward-symbol (- (or arg 1))))

(defun my/quote-symbol (quote reverse)
  "Wrap symbol at point (previous if REVERSE) between QUOTE chars."
  (if reverse (forward-symbol -1) (forward-symbol 1))
  (insert-char quote)
  (if reverse (forward-symbol 1) (forward-symbol -1))
  (insert-char quote))

;;;###autoload
(defun my/single-quote-symbol (&optional reverse)
  "Wrap symbol at point (previous if REVERSE) between single quote chars."
  (interactive "P")
  (my/quote-symbol ?\' reverse))

;;;###autoload
(defun my/single-quote-previous-symbol ()
  "Wrap previous symbol between single quote chars."
  (interactive)
  (my/quote-symbol ?\' 1))

;;;###autoload
(defun my/double-quote-symbol (&optional reverse)
  "Wrap symbol at point (previous if REVERSE) between double quote chars."
  (interactive "P")
  (my/quote-symbol ?\" reverse))

;;;###autoload
(defun my/double-quote-previous-symbol ()
  "Wrap previous symbol between double quote chars."
  (interactive)
  (my/quote-symbol ?\" 1))
