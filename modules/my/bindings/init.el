;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — Initialization
;; :Created:   mer 15 dic 2021, 12:18:00
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2021 Lele Gaifax
;;

(require 'ring)

;; Backward compatibility with my old emacs-starter-kit

(defvaralias 'esk/git-grep-exclusions 'my+git/grep-exclusions)
(put 'esk/git-grep-exclusions 'safe-local-variable 'listp)

(defvar my+git/grep-exclusions nil
  "A list of path exclusion patterns, added at the end of the `git grep' command.

It is marked as a `safe-local-variable' so you can define it in a
directory or file local variable. This is an actual example of
the former:

    ((nil . ((my+git/grep-exclusions . (\"**/*.min.js\" \"**/*.js.map\")))))

to ignore minified JS bundles and map files.

Look up `pathspec' in the `git help glossary' for details.")

(put 'my+git/grep-exclusions 'safe-local-variable 'listp)

;; Uncomment/add desired languages. Alternatively, specific users may
;; augment the ring adding something like
;;
;;  (ring-insert+extend my+ispell/languages "castellano8" t)
;;
;; to their user.el file

(defvar my+ispell/languages (ring-convert-sequence-to-ring
                             '("american"
                               "italiano"
                               ;; "castellano8"
                               ;; "deutsch8"
                               ;; "francais"
                               ;; "brasileiro"
                               ))
  "A ring containing the desired languages.

It is used by `my+ispell/cycle-languages'.")

(setq! ispell-dictionary "american")
