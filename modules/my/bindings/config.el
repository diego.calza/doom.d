;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — My bindings configuration
;; :Created:   ven 10 dic 2021, 13:09:49
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2021, 2022 Lele Gaifax
;;

(when (featurep! +fkeys)
  (use-package! iedit
    :init
    ;; Avoid default bindings, they seems weird to me
    (setq! iedit-toggle-key-default nil))
  (load! "+fkeys"))

(when (doom-module-p :completion 'vertico)
  ;; Rectify PgUp/PgDn effect, that by default operate on the history, not on
  ;; the listing
  (after! vertico
    (keymap-set vertico-map "<prior>" #'vertico-scroll-down)
    (keymap-set vertico-map "<next>" #'vertico-scroll-up)))

(map!
 :leader
 :prefix "t"
 :desc "Menu bar"                    "m"   #'menu-bar-mode
 :desc "Switch between H/V layout"   "S"   #'my/toggle-hv-window-layout
 :desc "Switch to alternative theme" "T"   #'my/switch-to-other-theme
 :desc "Follow mode"                 "C-f" #'follow-mode
 :desc "Scroll all mode"             "C-s" #'scroll-all-mode)

;; I miss this, for some reason registered only in the +evil case
(map!
 :leader
 :prefix "p"
 :desc "Add new project" "A" #'projectile-add-known-project)

(keymap-global-set "C-<left>"    #'backward-word)
(keymap-global-set "C-<right>"   #'forward-word)
(keymap-global-set "C-S-<left>"  #'backward-symbol)
(keymap-global-set "C-S-<right>" #'forward-symbol)

(keymap-global-set "C-o" #'my/open-next-line)
(keymap-global-set "M-o" #'my/open-previous-line)

(keymap-global-set "M-Z" #'zap-up-to-char)
(when (display-graphic-p)
  (keymap-global-set "C-z" #'zap-up-to-char))

;;
;; Switch to other windows with S-<cursor>
;;
(when (featurep! :my bindings +windmove)
  (windmove-default-keybindings))

;;
;; Make windmove work in Org mode too
;;
(after! (windmove org)
  (add-hook 'org-shiftup-final-hook    #'windmove-up)
  (add-hook 'org-shiftleft-final-hook  #'windmove-left)
  (add-hook 'org-shiftdown-final-hook  #'windmove-down)
  (add-hook 'org-shiftright-final-hook #'windmove-right))

;;
;; The following are bound to drag-stuff by default, but my fingers are trained
;; differently...
;;
(keymap-global-set "M-<up>"    #'my/scroll-up-one-line)
(keymap-global-set "M-<down>"  #'my/scroll-down-one-line)

(keymap-global-set "C-<prior>"  #'previous-buffer)
(keymap-global-set "C-<next>" #'next-buffer)
;; These are by historical bindings, but org-mode maps
;; them to structure edit
(keymap-global-set "M-<left>"  #'previous-buffer)
(keymap-global-set "M-<right>" #'next-buffer)

(use-package! drag-stuff
  :defer t
  :init
  (keymap-global-set "M-S-<up>"    #'drag-stuff-up)
  (keymap-global-set "M-S-<down>"  #'drag-stuff-down)
  (keymap-global-set "M-S-<left>"  #'drag-stuff-left)
  (keymap-global-set "M-S-<right>" #'drag-stuff-right))

;;
;; Wrap current/previous symbol with single/double quotes
;;
(keymap-global-set "C-'"  #'my/single-quote-symbol)
(keymap-global-set "M-'"  #'my/single-quote-previous-symbol)
(keymap-global-set "C-\"" #'my/double-quote-symbol)
(keymap-global-set "M-\"" #'my/double-quote-previous-symbol)

;;
;; Adjust things under macOS to do the right thing
;;
(when IS-MAC
  ;; Rectify behaviour of home/end
  (keymap-global-set "<home>" #'move-beginning-of-line)
  (keymap-global-set "<end>" #'move-end-of-line)
  ;; Restore standard meaning of Command-X
  (keymap-global-set "s-x" #'kill-region))

;;
;; Use the usual "read-only-mode" binding also for wrep
;;
(after! wgrep
  (setq! wgrep-enable-key "\C-x\C-q"))

;;
;; Revert this to the default: for some reason it is set to nil in the vertico
;; configuration, but I fail to see how one can obtain the equivalent, in
;; particular switch to further pages...
;;
(setq which-key-use-C-h-commands t)

;;
;; Split Python's string literals
;;
(when (featurep! :lang python)
  (after! python
    (map! :map 'python-mode-map
          "C-<return>" #'my+python/split-string)))

(use-package! google-translate
  :demand t
  :config
  ;; See bug https://github.com/atykhonov/google-translate/issues/137
  (defun google-translate--search-tkk ()
    "Search TKK."
    (list 430675 2721866130))

  (defun my/google-translate-at-point()
    "Translate region or word at point, reverse with prefix argument."
    (interactive)
    (if current-prefix-arg
        (google-translate-at-point-reverse)
      (google-translate-at-point)))

  (defun my/google-translate-query()
    "Prompt for sentence to translate, reverse with prefix argument."
    (interactive)
    (if current-prefix-arg
        (google-translate-query-translate-reverse)
      (google-translate-query-translate)))

  (when (featurep! +fkeys)
    (keymap-global-set "S-<f8>" #'my/google-translate-at-point)
    (keymap-global-set "C-<f8>" #'my/google-translate-query))

  (map!
   :leader
   :prefix "t"
   :desc "Translate at point" "." #'my/google-translate-at-point
   :desc "Translate query" "t" #'my/google-translate-query)

  (setq! google-translate-backend-method 'curl
         google-translate-default-source-language "en"
         google-translate-default-target-language "it"))
