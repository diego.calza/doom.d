;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d —
;; :Created:   lun 20 dic 2021, 08:06:34
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2021 Lele Gaifax
;;

(use-package! gnus
  :config

  (setq!
   ;; Store all Gnus stuff under a directory ignored by git
   gnus-home-directory (concat doom-local-dir "gnus/")
   ;; Customize summary format
   gnus-summary-line-format "%U%R%z%d %(%[%-20,20a%]%) %I%s\n"
   ;; Archive - putting sent mail and news somewhere
   gnus-message-archive-group '((if (message-news-p)
                                    (concat "sent-news-"
                                            (format-time-string "%b-%y"))
                                  (concat "sent-mail-"
                                          (format-time-string "%b-%y")))))

  (add-hook 'gnus-mode-hook #'my+gnus/add-buffer-to-persp)
  (add-hook 'message-mode-hook (lambda ()
                                 (when (eq major-mode 'message-mode)
                                   (my+gnus/add-buffer-to-persp))))

  ;; Automatically commit the dribble file when Emacs exits
  (add-hook 'kill-emacs-hook #'my+gnus/grace-exit-before-kill-emacs)

  ;; Treat Gnus buffers as real ones
  (add-hook 'doom-real-buffer-functions #'my+gnus/buffer-p)

  (map! :leader
        (:prefix-map ("G" . "gnus")
         :desc "Browse newsgroups" "G" #'my/gnus
         :desc "Quit gnus"         "q" #'gnus-group-exit)))

(use-package! gnus-start
  :config
  (setq!
   ;; By default the following files are "hidden" (begin with a dot),
   ;; but that's pointless since they live in a specific local dir
   gnus-init-file (concat gnus-home-directory "init")
   gnus-startup-file (concat gnus-home-directory "newsrc")
   ;; Since I only use Gnus to read the newsgroups, disable reading and
   ;; writing the generic newsrc file
   gnus-read-newsrc-file nil
   gnus-save-newsrc-file nil
   ;; Also, I manually select which newsgroups I'm gonna follow, so don't
   ;; bother with keeping a notion of "new" newsgroups
   gnus-save-killed-list nil
   gnus-check-new-newsgroups nil))

(use-package! gnus-group
  :config
  ;; Customize appearence
  (setq! gnus-permanently-visible-groups "."))

(use-package! gnus-sum
  :config
  ;; Customize summary format
  (setq! gnus-summary-same-subject "⤷"))

(use-package! mm-decode
  :config
  ;; Prefer plain text alternative
  (setq! mm-discouraged-alternatives '("text/html" "text/richtext" "image/.*")
         mm-automatic-display '("text/dns"
                                "text/enriched"
                                ;; "text/html"
                                "text/plain"
                                "text/richtext"
                                "text/x-org"
                                "text/x-patch"
                                "text/x-vcard"
                                "text/x-verbatim"
                                "image/.*"
                                "message/delivery-status"
                                "multipart/.*"
                                "message/rfc822"
                                "application/emacs-lisp"
                                "application/pgp-signature"
                                "application/pkcs7-mime"
                                "application/pkcs7-signature"
                                "application/x-emacs-lisp"
                                "application/x-pkcs7-mime"
                                "application/x-pkcs7-signature"
                                ;; Mutt still uses this even though it has
                                ;; already been withdrawn.
                                "application/pgp\\'")))

(use-package! gnus-art
  :config
  (setq! gnus-buttonized-mime-types '("multipart/alternative"
                                      "multipart/signed")))
