;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — Gnus helpers
;; :Created:   lun 20 dic 2021, 08:05:53
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2021, 2022 Lele Gaifax
;;

(defvar my+gnus/workspace-name "*GNUS*")

;;;###autoload
(defun my/gnus ()
  "Activate (or switch to) `gnus' in its workspace."
  (interactive)
  (when (featurep! :ui workspaces)
    (+workspace-switch my+gnus/workspace-name 'auto-create))
  (if-let* ((win (cl-find-if (lambda (it)
                               (string-match-p "^\\*Group\\*$"
                                               (buffer-name (window-buffer it))))
                             (doom-visible-windows))))
      (select-window win)
    (gnus))
  (when (featurep! :ui workspaces)
    (+workspace/display)
    (add-hook 'gnus-after-exiting-gnus-hook
              (lambda ()
                (+workspace/delete my+gnus/workspace-name)))))

;;;###autoload
(defun my+gnus/grace-exit-before-kill-emacs ()
  "Shutdown gnus if active, when exiting emacs."
  (if (and (fboundp 'gnus-alive-p)
           (gnus-alive-p))
      (let ((noninteractive t))
        (gnus-group-exit))))

;;;###autoload
(defun my+gnus/buffer-p (buf)
  "Return non-nil if BUF is a `gnus-mode' buffer."
  (with-current-buffer buf
    (derived-mode-p 'gnus-mode)))

;;;###autoload
(defun my+gnus/add-buffer-to-persp ()
  "Add gnus buffers to the `*GNUS*' workspace."
  (when (and (bound-and-true-p persp-mode)
             (+workspace-exists-p my+gnus/workspace-name))
    (let ((ws (persp-get-by-name my+gnus/workspace-name))
          (buf (current-buffer)))
      ;; Add a new gnus buffer to the right workspace if not already there
      (unless (cl-find buf (safe-persp-buffers ws))
        (persp-add-buffer buf ws nil nil)))))
