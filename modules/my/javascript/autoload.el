;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — JS helpers
;; :Created:   dom 26 dic 2021, 10:06:05
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2021 Lele Gaifax
;;

;;;###autoload
(defun my+js2/apply-jsl-declares ()
  "Extract top level //jsl:declare XXX comments"
  (setq js2-additional-externs
        (nconc (my+js2/get-jsl-declares)
               js2-additional-externs)))

;;;###autoload
(defun my+js2/get-jsl-declares ()
  (cl-loop for node in (js2-ast-root-comments js2-mode-ast)
           when (and (js2-comment-node-p node)
                     (save-excursion
                       (goto-char (+ 2 (js2-node-abs-pos node)))
                       (looking-at "jsl:declare ")))
           append (my+js2/get-jsl-declares-in
                   (match-end 0)
                   (js2-node-abs-end node))))

;;;###autoload
(defun my+js2/get-jsl-declares-in (beg end)
  (let (res)
    (save-excursion
      (goto-char beg)
      (while (re-search-forward js2-mode-identifier-re end t)
        (push (match-string-no-properties 0) res)))
    (nreverse res)))

;;;###autoload
(defun my+js2/errors-and-warnings ()
  (let ((errors (length js2-parsed-errors))
        (warnings (length js2-parsed-warnings))
        result)
    (when (> errors 0)
      (push (propertize (format "%dE" errors) 'face 'error)
            result))
    (when (> warnings 0)
      (push (propertize (format "%dW" warnings) 'face 'warning)
            result))
    (propertize (mapconcat 'identity result " ")
                'help-echo (format "mouse-1: List all problems\nmouse-3: Next problem%s"
                                   (if (featurep 'mwheel)
                                       "\nwheel-up/wheel-down: Previous/next problem"))
                'local-map
                (let ((map (make-sparse-keymap)))
                  (define-key map [mode-line mouse-1]
                    #'js2-display-error-list)
                  (define-key map [mode-line mouse-3]
                    #'js2-next-error)
                  (when (featurep 'mwheel)
                    (define-key map (vector 'mode-line mouse-wheel-down-event)
                      (lambda (event)
                        (interactive "e")
                        (with-selected-window (posn-window (event-start event))
                          (js2-next-error -1))))
                    (define-key map (vector 'mode-line mouse-wheel-up-event)
                      (lambda (event)
                        (interactive "e")
                        (with-selected-window (posn-window (event-start event))
                          (js2-next-error)))))
                  map))))
