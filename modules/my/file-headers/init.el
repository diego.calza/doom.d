;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — File headers initialization
;; :Created:   ven 10 dic 2021, 13:17:04
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2021 Lele Gaifax
;;

;; The project name, license and copyright holder can be easily customized within a
;; .dir-locals.el file, for example:
;;
;;  ((nil . ((my+fh/project-name . "my-project")
;;           (my+fh/project-license . "MIT License")
;;           (my+fh/project-copyright-holder . "Arstecnica s.r.l."))))
;;

;; Backward compatibility with my old emacs-starter-kit

(defvaralias 'esk/project-name 'my+fh/project-name)
(put 'esk/project-name 'safe-local-variable 'stringp)

(defvaralias 'esk/project-license 'my+fh/project-license)
(put 'esk/project-license 'safe-local-variable 'stringp)

(defvaralias 'esk/project-copyright-holder 'my+fh/project-copyright-holder)
(put 'esk/project-copyright-holder 'safe-local-variable 'stringp)

(defvar my+fh/project-name nil
  "Last project name.")

(put 'my+fh/project-name 'safe-local-variable 'stringp)

(defun my+fh/project-name ()
  (setq my+fh/project-name (read-string "Project: " my+fh/project-name)))

(defvar my+fh/project-license "GNU General Public License version 3 or later"
  "Last project license.")

(put 'my+fh/project-license 'safe-local-variable 'stringp)

(defun my+fh/project-license ()
  (setq my+fh/project-license (read-string "License: " my+fh/project-license)))

(defvar my+fh/project-copyright-holder (user-full-name)
  "Last project copyright holder")

(put 'my+fh/project-copyright-holder 'safe-local-variable 'stringp)

(defun my+fh/project-copyright-holder ()
  (setq my+fh/project-copyright-holder
        (read-string "Copyright holder: " my+fh/project-copyright-holder)))
