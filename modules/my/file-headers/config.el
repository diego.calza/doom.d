;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — File headers configuration
;; :Created:   ven 10 dic 2021, 13:16:43
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2021, 2022 Lele Gaifax
;;

(use-package! autoinsert
  :config
  (define-skeleton my+fh/*
    "Generic file header."
    "Summary: "
    comment-start `(delete-horizontal-space) " -*- coding: utf-8 -*-" comment-end "\n"
    comment-start `(delete-horizontal-space) " :Project:   " (my+fh/project-name) " — " str "\n"
    comment-start `(delete-horizontal-space) " :Created:   " (format-time-string "%c") comment-end "\n"
    comment-start `(delete-horizontal-space) " :Author:    " (user-full-name) " <" user-mail-address ">" comment-end "\n"
    comment-start `(delete-horizontal-space) " :License:   " (my+fh/project-license) comment-end "\n"
    comment-start `(delete-horizontal-space) " :Copyright: © " (format-time-string "%Y") " " (my+fh/project-copyright-holder) comment-end "\n"
    comment-start `(delete-horizontal-space) comment-end "\n\n")

  (define-skeleton my+fh/block
    "Block comment file header."
    "Summary: "
    comment-start `(delete-horizontal-space) " -*- coding: utf-8 -*-\n"
    " * :Project:   " (my+fh/project-name) " — " str "\n"
    " * :Created:   " (format-time-string "%c") "\n"
    " * :Author:    " (user-full-name) " <" user-mail-address ">\n"
    " * :License:   " (my+fh/project-license) "\n"
    " * :Copyright: © " (format-time-string "%Y") " " (my+fh/project-copyright-holder) "\n"
    " " `(delete-horizontal-space) comment-end "\n\n")

  (define-skeleton my+fh/html
    "HTML file header."
    "Summary: "
    comment-start `(delete-horizontal-space) " -*- coding: utf-8 -*-\n"
    "---- :Project:   " (my+fh/project-name) " — " str "\n"
    "---- :Created:   " (format-time-string "%c") "\n"
    "---- :Author:    " (user-full-name) " <" user-mail-address ">\n"
    "---- :License:   " (my+fh/project-license) "\n"
    "---- :Copyright: © " (format-time-string "%Y") " " (my+fh/project-copyright-holder) "\n"
    "-" `(delete-horizontal-space) comment-end "\n\n")

  (define-skeleton my+fh/el
    "Emacs Lisp file header."
    "Summary: "
    ";;; -*- coding: utf-8; lexical-binding: t; -*-\n"
    ";; :Project:   " (my+fh/project-name) " — " str "\n"
    ";; :Created:   " (format-time-string "%c") "\n"
    ";; :Author:    " (user-full-name) " <" user-mail-address ">" "\n"
    ";; :License:   " (my+fh/project-license) "\n"
    ";; :Copyright: © " (format-time-string "%Y") " " (my+fh/project-copyright-holder) "\n"
    ";;\n\n")

  (define-skeleton my+fh/mako
    "Mako file header."
    "Summary: "
    "## -*- coding: utf-8 -*-\n"
    "## :Project:   " (my+fh/project-name) " — " str "\n"
    "## :Created:   " (format-time-string "%c") "\n"
    "## :Author:    " (user-full-name) " <" user-mail-address ">\n"
    "## :License:   " (my+fh/project-license) "\n"
    "## :Copyright: " "© " (format-time-string "%Y") " " (my+fh/project-copyright-holder) "\n"
    "##\n\n")

  (define-skeleton my+fh/jinja2
    "Jinja2 file header."
    "Summary: "
    "{# -*- coding: utf-8 -*-\n"
    "## :Project:   " (my+fh/project-name) " — " str "\n"
    "## :Created:   " (format-time-string "%c") "\n"
    "## :Author:    " (user-full-name) " <" user-mail-address ">\n"
    "## :License:   " (my+fh/project-license) "\n"
    "## :Copyright: " "© " (format-time-string "%Y") " " (my+fh/project-copyright-holder) "\n"
    "#}\n\n")

  (define-skeleton my+fh/sql
    "SQL file header."
    "Summary: "
    comment-start `(delete-horizontal-space) " -*- coding: utf-8; sql-product: " (symbol-name (sql-read-product "SQL product: ")) " -*-" comment-end "\n"
    comment-start `(delete-horizontal-space) " :Project:   " (my+fh/project-name) " — " str comment-end "\n"
    comment-start `(delete-horizontal-space) " :Created:   " (format-time-string "%c") comment-end "\n"
    comment-start `(delete-horizontal-space) " :Author:    " (user-full-name) " <" user-mail-address ">" comment-end "\n"
    comment-start `(delete-horizontal-space) " :License:   " (my+fh/project-license) comment-end "\n"
    comment-start `(delete-horizontal-space) " :Copyright: © " (format-time-string "%Y") " " (my+fh/project-copyright-holder) comment-end "\n"
    comment-start `(delete-horizontal-space) comment-end "\n\n")

  ;; Endian
  ;;;;;;;;;;

  (define-skeleton my+fh+endian/*
    "Generic Endian file header."
    "Summary: "
    comment-start `(delete-horizontal-space) " -*- coding: utf-8 -*-" comment-end "\n"
    comment-start `(delete-horizontal-space) " +--------------------------------------------------------------------------+" comment-end "\n"
    comment-start `(delete-horizontal-space) " | " (format "%-73s" (my+fh/project-name)) "|" comment-end "\n"
    comment-start `(delete-horizontal-space) " +--------------------------------------------------------------------------+" comment-end "\n"
    comment-start `(delete-horizontal-space) " | Copyright (c) 2004-" (format-time-string "%Y") " Endian Srl <info@endian.com>                     |" comment-end "\n"
    comment-start `(delete-horizontal-space) " |         Endian Srl                                                       |" comment-end "\n"
    comment-start `(delete-horizontal-space) " |         via Ipazia 2                                                     |" comment-end "\n"
    comment-start `(delete-horizontal-space) " |         39100 Bolzano (BZ)                                               |" comment-end "\n"
    comment-start `(delete-horizontal-space) " |         Italy                                                            |" comment-end "\n"
    comment-start `(delete-horizontal-space) " |                                                                          |" comment-end "\n"
    comment-start `(delete-horizontal-space) " | This program is proprietary software; you are not allowed to             |" comment-end "\n"
    comment-start `(delete-horizontal-space) " | redistribute and/or modify it.                                           |" comment-end "\n"
    comment-start `(delete-horizontal-space) " | This program is distributed in the hope that it will be useful,          |" comment-end "\n"
    comment-start `(delete-horizontal-space) " | but WITHOUT ANY WARRANTY; without even the implied warranty of           |" comment-end "\n"
    comment-start `(delete-horizontal-space) " | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     |" comment-end "\n"
    comment-start `(delete-horizontal-space) " +--------------------------------------------------------------------------+" comment-end "\n"
    comment-start `(delete-horizontal-space) comment-end "\n"
    comment-start `(delete-horizontal-space) " :Created:   " (current-time-string) comment-end "\n"
    comment-start `(delete-horizontal-space) " :Author:    " (user-full-name) " <" user-mail-address ">" comment-end "\n"
    comment-start `(delete-horizontal-space) comment-end "\n\n")

  (define-skeleton my+fh+endian/block
    "Block comment file header."
    "Summary: "
    comment-start `(delete-horizontal-space) " -*- coding: utf-8 -*-\n"
    " * +--------------------------------------------------------------------------+\n"
    " * | " (format "%-73s" (my+fh/project-name)) "|\n"
    " * +--------------------------------------------------------------------------+\n"
    " * | Copyright (c) 2004-" (format-time-string "%Y") " Endian Srl <info@endian.com>                     |\n"
    " * |         Endian Srl                                                       |\n"
    " * |         via Ipazia 2                                                     |\n"
    " * |         39100 Bolzano (BZ)                                               |\n"
    " * |         Italy                                                            |\n"
    " * |                                                                          |\n"
    " * | This program is proprietary software; you are not allowed to             |\n"
    " * | redistribute and/or modify it.                                           |\n"
    " * | This program is distributed in the hope that it will be useful,          |\n"
    " * | but WITHOUT ANY WARRANTY; without even the implied warranty of           |\n"
    " * | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     |\n"
    " * +--------------------------------------------------------------------------+\n"
    " *\n"
    " * :Created:   " (current-time-string) "\n"
    " * :Author:    " (user-full-name) " <" user-mail-address ">\n"
    " " `(delete-horizontal-space) comment-end "\n\n")

  ;; NOTE: first match wins, so order is important!

  (setq! auto-insert-alist
         '(("/endian/" . my+fh+endian/*)

           (c-mode . my+fh/block)
           (emacs-lisp-mode . my+fh/el)
           (("\\.html\\'" . "HTML") . my+fh/html)
           (("\\.jinja2\\'" . "Jinja2") . my+fh/jinja2)
           (("\\.mako\\'" . "Mako") . my+fh/mako)
           (sql-mode . my+fh/sql)

           (prog-mode . my+fh/*)
           (text-mode . my+fh/*)))

  (auto-insert-mode 1)
)

(when (featurep! +copyright-years)
  ;; Automatically update copyright years when saving
  (use-package! copyright
    :config
    (setq! copyright-names-regexp (regexp-quote (user-full-name))
           copyright-year-ranges t)
    (defun my+fh/copyright-update ()
      (when copyright-update
        (let ((copyright-names-regexp (concat
                                       "\\(" copyright-names-regexp
                                       "\\)\\|\\("
                                       (regexp-quote my+fh/project-copyright-holder)
                                       "\\)")))
          (copyright-update))))
    (add-hook 'before-save-hook #'my+fh/copyright-update)))
