;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — Defaults packages
;; :Created:   ven 10 dic 2021, 13:14:03
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2021, 2022 Lele Gaifax
;;

(package! subword :built-in t)
(package! whitespace :built-in t)

;; Disable JS tide, does not work for me...
(package! tide :disable t)

;; Disable savehist, I do not find any value in it
(package! savehist :disable t)

(package! po-mode
  :recipe `(:host github
            :repo "lelit/po-mode"
            :branch "my-hacks"
            :files ("po-mode.el"))
  :pin "17478f84677f96e10b099fe1dcc38f1255f7546d")

(package! pycov-mode
  :recipe `(:host github
            :repo "lelit/pycov-mode")
  :pin "5cbae4abbc7bc4b550814f92544fb1e6bf0f2443")

(package! page-break-lines
  :pin "28783cd6b86b3cd41e51e6b2486173e2485a76cc")

(package! magit-delta
  :pin "5fc7dbddcfacfe46d3fd876172ad02a9ab6ac616")

(package! lorem-ipsum
  :pin "da75c155da327c7a7aedb80f5cfe409984787049")

(package! kivy-mode
  :pin "b692b227637d0ffb8aedaa602e67d5a22af23551")

(package! vbnet-mode
  :recipe `(:host github
            :repo "lelit/vbnet-mode")
  :pin "416db89933e15609603d43d526dc7c969c343715")

(package! just-mode
  :pin "8cf9e686c8c7bb725c724b5220a4a3ed17d005d0")
