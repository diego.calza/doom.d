;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — Diego's preferences
;; :Created:   mar 21 dic 2021, 11:42:08
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2021 Lele Gaifax
;;

(doom!
 :completion
 (vertico +icons)

 :ui
 deft
 (emoji +unicode)
 indent-guides
 nav-flash
 (treemacs +lsp)

 :emacs
 (dired +icons +ranger)

 :checkers
 (spell +flyspell +aspell)

 :tools
 lsp
 pdf

 :lang
 javascript
 json
 (org +pretty +brain)
 plantuml
 (python +lsp +pyright +cython)
 rst
 web
 yaml

 :app
 calendar
 irc
 )
