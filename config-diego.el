(setq! user-full-name "Diego Calzà"
       user-mail-address "diego@etour.tn.it")

(setq! doom-theme 'doom-one-light)

;; Alternative theme, activable with `C-c t T'
(setq! my/other-theme 'doom-one)


(setq! org-directory "~/Dropbox/org/")
(setq! deft-directory "~/Dropbox/org/notes")

;; Append "/usr/local/bin" to $PATH, so Emacs can find homebrew-installed tools
(setenv "PATH" (concat (getenv "PATH") ":/usr/local/bin"))

;;
;; Elfeed feeds setting
;;
(use-package! elfeed
  :custom
  (elfeed-feeds '(
                "https://planetpython.org/rss20.xml"
                "https://go.indiegogo.com/feed"
                  )
                )
  )

;;
;; IRC
;;
(after! circe
  (set-irc-server! "irc.libera.chat"
    `(:tls t
      :port 6697
      :nick "diego"
      :channels ("#rafanass"
                 "#etour"))))

(keymap-global-set "s-x" #'kill-region)

(after! plantuml-mode
  (setq! plantuml-jar-path "/usr/local/Cellar/plantuml/1.2023.6/libexec/plantuml.jar"))

(after! ox-latex
  (setq! org-latex-caption-above nil))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-dracula) ;; << This line enables the theme


;;
;; Translate with Deepl
;;
;; (use-package! txl
;;   :config
;;   (setq txl-languages '(IT . EN-US)
;;         txl-deepl-api-key "dabbee01-e727-6be0-3d45-e579b6bb219b")
;;   (keymap-global-set "C-c t d" #'txl-translate-region-or-paragraph))

(use-package! go-translate
  :config
  (setq
   gts-translate-list '(("en" "it") ("it" "en"))
   gts-default-translator (gts-translator
                           :picker (gts-prompt-picker)
                           :engines (gts-deepl-engine :auth-key "dabbee01-e727-6be0-3d45-e579b6bb219b" :pro t)
                           :render (gts-buffer-render)))
  (keymap-global-set "C-c t d" #'gts-do-translate))

(use-package! cua-base
  :config
  (cua-mode t))

(use-package! menu-bar
  :config
  (menu-bar-mode t))
